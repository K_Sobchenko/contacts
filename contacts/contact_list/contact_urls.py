# -*- coding: utf-8 -*-
from django.conf.urls import url
from .contact_views import ContactMainView, AddContact, DetailContact, EditContact, \
                           DeleteContact, Export, Import, DeleteAllContacts, UploadImportXML


urlpatterns = [
    url(r'main', ContactMainView.as_view(), name='contacts_main_page'),
    url(r'add', AddContact.as_view(), name='contacts_add'),
    url(r'detail/(?P<pk>\d+)/$', DetailContact.as_view(), name='contacts_detail'),
    url(r'edit/(?P<pk>\d+)/$', EditContact.as_view(), name='contacts_edit'),
    url(r'delete/(?P<pk>\d+)/$', DeleteContact.as_view(), name='contacts_delete'),
    url(r'export', Export.as_view(), name='contacts_export'),
    url(r'import', Import.as_view(), name='contacts_import'),
    url(r'delete-all', DeleteAllContacts.as_view(), name='delete_all'),
    url(r'upload-xml', UploadImportXML.as_view(), name='upload_xml'),
]
