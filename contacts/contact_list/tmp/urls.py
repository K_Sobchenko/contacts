# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import AddFish, MainPage, EditFish, DeleteFish, DetailFish, \
    BaitPage, DeleteBait, AddBait, EditBait, \
    LurePade, AddLure, EditLure, DeleteLure, \
    FishLurePade, FishAddLure, FishEditLure, FishDeleteLure, \
    FishBaitPage, FishDeleteBait, FishAddBait, FishEditBait


urlpatterns = [

    url(r'add-fishbait', FishAddBait.as_view(), name='add_fishbait'),
    url(r'edit-fishbait/(?P<pk>\d+)/$', FishEditBait.as_view(), name='edit_fishbait'),
    url(r'delete-fishbait/(?P<pk>\d+)/$', FishDeleteBait.as_view(), name='delete_fishbait'),
    url(r'fishbait', FishBaitPage.as_view(), name='fishbait'),

    url(r'add-fishlure', FishAddLure.as_view(), name='add_fishlure'),
    url(r'edit-fishlure/(?P<pk>\d+)/$', FishEditLure.as_view(), name='edit_fishlure'),
    url(r'delete-fishlure/(?P<pk>\d+)/$', FishDeleteLure.as_view(), name='delete_fishlure'),
    url(r'fishlure', FishLurePade.as_view(), name='fishlure'),

    url(r'add-lure', AddLure.as_view(), name='add_lure'),
    url(r'edit-lure/(?P<pk>\d+)/$', EditLure.as_view(), name='edit_lure'),
    url(r'delete-lure/(?P<pk>\d+)/$', DeleteLure.as_view(), name='delete_lure'),
    url(r'lure', LurePade.as_view(), name='lure'),

    url(r'add-bait', AddBait.as_view(), name='add_bait'),
    url(r'edit-bait/(?P<pk>\d+)/$', EditBait.as_view(), name='edit_bait'),
    url(r'delete-bait/(?P<pk>\d+)/$', DeleteBait.as_view(), name='delete_bait'),
    url(r'bait', BaitPage.as_view(), name='bait'),

    url(r'add', AddFish.as_view(), name='add_fish'),
    url(r'edit/(?P<pk>\d+)/$', EditFish.as_view(), name='edit_fish'),
    url(r'delete/(?P<pk>\d+)/$', DeleteFish.as_view(), name='delete_fish'),
    url(r'detail/(?P<pk>\d+)/$', DetailFish.as_view(), name='detail_fish'),
    url(r'^$', MainPage.as_view(), name='main'),
]
