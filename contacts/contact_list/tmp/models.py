# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class MetaBase():
    app_label = 'contact_list'


class Fish(models.Model):
    fish_id = models.AutoField(primary_key=True)
    start_fishing_date = models.DateField(blank=True, null=True, verbose_name='Дата начала клева')
    finish_fishing_date = models.DateField(blank=True, null=True, verbose_name='Дата конца клева')
    start_spawning_date = models.DateField(blank=True, null=True, verbose_name='Дата начала нереста')
    finish_spawning_date = models.DateField(blank=True, null=True, verbose_name='Дата конца нереста')
    start_fishing_time = models.TimeField(blank=True, null=True, verbose_name='Время начала лова')
    finish_fishing_time = models.TimeField(blank=True, null=True, verbose_name='Время конца лова')
    description = models.TextField(blank=True, null=True, verbose_name='Описание')
    name = models.CharField(max_length=100, blank=True, null=False, verbose_name='Название')
    is_red = models.BooleanField(blank=True, verbose_name='Красная')
    class Meta(MetaBase):
        db_table = 'fish'

    def __unicode__(self):
        return u'%s' % self.name

class Bait(models.Model):
    bait_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True, null=False, verbose_name='Наживка')
    class Meta(MetaBase):
        db_table = 'bait'

    def __unicode__(self):
        return u'%s' % self.title

class FishBait(models.Model):
    fish_bait_id = models.AutoField(primary_key=True)
    fish = models.ForeignKey('Fish', blank=True, null=False, verbose_name='Рыба')
    bait = models.ForeignKey('Bait', blank=True, null=False, verbose_name='Наживка')
    class Meta(MetaBase):
        db_table = 'fish_bait'

    def __unicode__(self):
        return u'%s - %s' % (self.fish.name, self.bait.title)

class Lure(models.Model):
    lure_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True, null=False, verbose_name='Прикормка')
    class Meta(MetaBase):
        db_table = 'lure'

    def __unicode__(self):
        return u'%s' % self.title

class FishLure(models.Model):
    fish_lure_id = models.AutoField(primary_key=True)
    fish = models.ForeignKey('Fish', blank=True, null=False, verbose_name='Рыба')
    lure = models.ForeignKey('Lure', blank=True, null=False, verbose_name='Прикормка')
    class Meta(MetaBase):
        db_table = 'fish_lure'

    def __unicode__(self):
        return u'%s - %s' % (self.fish.name, self.lure.title)