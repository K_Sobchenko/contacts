# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, NullBooleanSelect
from django.forms.extras.widgets import SelectDateWidget
from .models import Fish, Bait, Lure, FishBait, FishLure
from .widgets import SelectTimeWidget


class FishForm(forms.ModelForm):
    start_fishing_date = forms.DateField(label='Дата начала клева', widget=SelectDateWidget(years=range(2000, 2001)))
    finish_fishing_date = forms.DateField(label='Дата конца клева', widget=SelectDateWidget(years=range(2000, 2001)))
    start_spawning_date = forms.DateField(label='Дата начала нереста', widget=SelectDateWidget(years=range(2000, 2001)))
    finish_spawning_date = forms.DateField(label='Дата конца нереста', widget=SelectDateWidget(years=range(2000, 2001)))
    start_fishing_time = forms.TimeField(label='Время начала лова', widget=SelectTimeWidget(minute_step=15, second_step=30, twelve_hr=True, use_seconds=False))
    finish_fishing_time = forms.TimeField(label='Время конца лова', widget=SelectTimeWidget(minute_step=15, second_step=30, twelve_hr=True, use_seconds=False))

    class Meta:
        model = Fish
        fields = ['start_fishing_date', 'finish_fishing_date', 'start_spawning_date', 'finish_spawning_date', 'start_fishing_time', 'finish_fishing_time', 'description', 'name', 'is_red']

class BaitForm(forms.ModelForm):
    class Meta:
        model = Bait
        fields = ['title']

class LureForm(forms.ModelForm):
    class Meta:
        model = Lure
        fields = ['title']

class FishBaitForm(forms.ModelForm):
    class Meta:
        model = FishBait
        fields = ['fish', 'bait']

class FishLureForm(forms.ModelForm):
    class Meta:
        model = FishLure
        fields = ['fish', 'lure']



def choose_bait():
    bait = Bait.objects.all()
    baits = [(b.bait_id, b.title) for b in bait]
    baits.insert(0,(u'0', u'Любая наживка'))
    return baits

def choose_lure():
    lure = Lure.objects.all()
    lures = [(l.lure_id, l.title) for l in lure]
    lures.insert(0,(u'0',u'Любая прикормка'))
    return lures

class SearchForm(forms.Form):
    fishing_date = forms.DateField(label=u'Дата клева', widget=SelectDateWidget(years=range(2000, 2001)))
    spawning_date = forms.DateField(label=u'Дата нереста', widget=SelectDateWidget(years=range(2000, 2001)))
    fishing_time = forms.TimeField(label=u'Время начала лова', widget=SelectTimeWidget(minute_step=15, second_step=30, twelve_hr=False, use_seconds=False))
    fl = forms.BooleanField(label=u'Любая дата лова', initial=True, required=False)
    fl2 = forms.BooleanField(label=u'Любое время', initial=True, required=False)
    fl3 = forms.BooleanField(label=u'Любая дата нереста', initial=True, required=False)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields[u'bait'] = forms.ChoiceField(label=u'Наживка:', widget=forms.Select, choices=choose_bait())
        self.fields[u'lure'] = forms.ChoiceField(label=u'Прикормка:', widget=forms.Select, choices=choose_lure())
        # self.fields[u'fl'].blank = True