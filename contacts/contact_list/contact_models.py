# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import base64

class MetaBase():
    app_label = 'contact_list'

class Contacts(models.Model):
    """
    a.	Полное имя – text
    b.	Организация – text
    c.	Должность -  text
    d.	Электронная почта
    e.	Краткое имя
    f.	Вебстраница
    g.	Телефон сотовый
    h.	Телефон домашний
    i.	Адрес домашний
    j.	Факс – многосторчный текст
    k.	Телефон рабочий
    l.	Адрес почтовый
    m.	Считать почтовым адресом (да/нет)
    n.	Заметки – многострочн. Текст
    o.	Фото – изображение
    p.	Карточка – дублирует основные поля контакта в компактном виде (поля интерфейса)
    """
    contact_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'ФИО')
    organization = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Организация')
    post = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Должность')
    email = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Почта') # добавить маску
    short_name = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Короткое имя')
    web_page = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Веб страница') # добавить маску
    phone = models.IntegerField(blank=False, null=False, verbose_name=u'Мобильный телефон') # добавить маску
    home_phone = models.IntegerField(blank=False, null=False, verbose_name=u'Домашний телефон') # добавить маску
    work_phone = models.IntegerField(blank=False, null=False, verbose_name=u'Рабочий телефон') # добавить маску
    adres = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Адрес') # добавить маску
    fax = models.IntegerField(max_length=100, blank=False, null=False, verbose_name=u'Факс') # добавить маску
    mail_adres = models.CharField(max_length=100, blank=False, null=False, verbose_name=u'Почтовый адрес') # добавить маску
    is_mail_adres = models.BooleanField(blank=False, verbose_name=u'Считать почтовым')
    details = models.TextField(blank=False, null=False, verbose_name=u'Заметки')
    photo = models.ImageField(blank=True, null=True, verbose_name=u'Фото', upload_to='images')
    photo_as_text = models.TextField(blank=True, null=True)

    class Meta(MetaBase):
        db_table = 'contacts'

    def save(self, *args, **kwargs):
        """
        При сохранении контакта если фото изменилось удаляем старое
        В поле photo_as_text сохраняем картинку в виде текста
        """
        try:
            this_record = Contacts.objects.get(pk = self.pk)
            if this_record.photo != self.photo:
                this_record.photo.delete(save = False)
        except:
            pass
        try:
            file = self.photo.file.file.read()
            self.photo_as_text = base64.encodestring(file)
        except:
            pass
        super(Contacts, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """
        При удалении контакта удаляем фото
        """
        try:
            this_record = Contacts.objects.get(pk = self.pk)
            this_record.photo.delete(save = False)
        except:
            pass
        super(Contacts, self).delete(*args, **kwargs)