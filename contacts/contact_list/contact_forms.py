# -*- coding: utf-8 -*-
from django import forms
from .contact_models import Contacts

class ContactForm(forms.ModelForm):
    # contact_id = forms.IntegerField()
    # name = forms.CharField(label='ФИО')
    # organization = forms.CharField(label='Организация')
    # post = forms.CharField(label='Должность')
    # email = forms.CharField(label='Почта') # добавить маску
    # short_name = forms.CharField(label='Короткое имя')
    # web_page = forms.CharField(label='Веб страница') # добавить маску
    # phone = forms.IntegerField(label='Мобильный телефон') # добавить маску
    # home_phone = forms.IntegerField(label='Домашний телефон') # добавить маску
    # work_phone = forms.IntegerField(label='Рабочий телефон') # добавить маску
    # adres = forms.CharField(label='Адрес') # добавить маску
    # fax = forms.IntegerField(label='Факс') # добавить маску
    # mail_adres = forms.CharField(label='Почтовый адрес') # добавить маску
    # is_mail_adres = forms.NullBooleanField(label='Считать почтовым', widget=forms.CheckboxInput)
    details = forms.CharField(label='Заметки', widget=forms.Textarea)

    class Meta:
        model = Contacts
        fields = ['contact_id', 'name', 'organization', 'post', 'email', 'short_name', 'web_page', 'phone', 'home_phone',
                  'work_phone', 'adres', 'fax', 'mail_adres', 'is_mail_adres', 'details', 'photo']