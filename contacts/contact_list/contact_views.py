# -*- coding: utf-8 -*-
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.views.generic import CreateView, TemplateView, UpdateView, View
from contacts.settings import MEDIA_URL, MEDIA_ROOT
from .contact_models import Contacts
from .contact_forms import ContactForm
import base64


class ContactMainView(TemplateView):
    """
    Главная страница - список контактов
    """
    template_name = 'contacts_main_page.html'

    def get_context_data(self, **kwargs):
        context = super(ContactMainView, self).get_context_data(**kwargs)
        context['contacts'] = Contacts.objects.all()
        return context


class AddContact(CreateView):
    """
    Добавление контакта
    """
    template_name = 'contacts_add.html'
    form_class = ContactForm
    model = Contacts

    def get_context_data(self, **kwargs):
        context = super(AddContact, self).get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return reverse('contacts_main_page')


class DetailContact(TemplateView):
    """
    Подробная информация о контакте
    """
    template_name = 'contact_detail.html'

    def get_context_data(self, **kwargs):
        context = super(DetailContact, self).get_context_data(**kwargs)
        context['contact'] = Contacts.objects.get(pk=self.kwargs['pk'])
        context['MEDIA_URL'] = MEDIA_URL
        return context


class EditContact(UpdateView):
    """
    Изменение контакта
    """
    template_name = 'contacts_edit.html'
    form_class = ContactForm
    model = Contacts

    def get_context_data(self, **kwargs):
        context = super(EditContact, self).get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return reverse('contacts_main_page')


class DeleteContact(View):
    def get(self, *args, **kwargs):
        contact = Contacts.objects.get(pk=self.kwargs['pk'])
        contact.delete()
        return HttpResponseRedirect(reverse('contacts_main_page'))


class Export(View):
    """
    Экспорт в XML
    """

    def get(self, *args, **kwargs):
        contacts = Contacts.objects.all()
        xml = serializers.serialize("xml", contacts)
        f = open(MEDIA_ROOT + '/export.xml', 'w')
        f.write(xml)
        f.close()
        return HttpResponse(xml, content_type='text/xml')


class Import(View):
    """
    Из в XML
    """

    def get(self, *args, **kwargs):
        contacts = Contacts.objects.all()
        for contact in contacts:
            contact.delete()
        f = open(MEDIA_ROOT + '/import.xml', 'r')
        for cd in serializers.deserialize("xml", f.read()):
            img = open(MEDIA_ROOT + '/' + cd.object.photo.name, 'w')
            img.write(base64.decodestring(cd.object.photo_as_text))
            img.close()
            cd.object.save()
        f.close()
        return HttpResponseRedirect(reverse('contacts_main_page'))


class DeleteAllContacts(View):
    """
    Удаление из БД всех контактов
    """

    def get(self, *args, **kwargs):
        contacts = Contacts.objects.all()
        for contact in contacts:
            contact.delete()
        return HttpResponseRedirect(reverse('contacts_main_page'))


class UploadImportXML(View):
    """
    Из в XML
    """

    def get(self, *args, **kwargs):
        return HttpResponseRedirect(reverse('contacts_main_page'))

    def post(self, request, *args, **kwagrs):
        f = open(MEDIA_ROOT + '/import.xml', 'w')
        try:
            f.write(self.request.FILES['xmlfile'].file.file.read())
        except:
            f.write(self.request.FILES['xmlfile'].file.getvalue())
        f.close()
        return HttpResponseRedirect(reverse('contacts_main_page'))
