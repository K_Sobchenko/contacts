# -*- coding: utf-8 -*-
from datetime import datetime, time
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic import CreateView, TemplateView, UpdateView, View, FormView
from .models import Fish, Bait, Lure, FishLure, FishBait
from .forms import FishForm, BaitForm, LureForm, FishLureForm, FishBaitForm, SearchForm
from django.core.exceptions import PermissionDenied
import  calendar


class MainPage(FormView):
    template_name = 'main.html'
    form_class = SearchForm
    post_context = {}

    def get_context_data(self, **kwargs):

        spawning_date_year = 2000
        spawning_date_month = self.post_context.get('spawning_date_month', datetime.today().month)
        spawning_date_day = self.post_context.get('spawning_date_day', datetime.today().day)
        fishing_date_year = 2000
        fishing_date_month = self.post_context.get('fishing_date_month', datetime.today().month)
        fishing_date_day = self.post_context.get('fishing_date_day', datetime.today().day)
        tmp = {}
        tmp['fishing_date'] = datetime(int(fishing_date_year), int(fishing_date_month), int(fishing_date_day), 0, 0, 0)
        tmp['spawning_date'] = datetime(int(spawning_date_year), int(spawning_date_month), int(spawning_date_day), 0, 0, 0)
        tmp['fishing_time'] = time(int(self.post_context.get('fishing_time_hour', 0)),int(self.post_context.get('fishing_time_minute', 0)))
        tmp['fl'] = self.post_context.get('fl', 'true')
        tmp['fl2'] = self.post_context.get('fl2', 'true')
        tmp['fl3'] = self.post_context.get('fl3', 'true')
        tmp['bait'] = self.post_context.get('bait', '0')
        tmp['lure'] = self.post_context.get('lure', '0')

        context = super(MainPage, self).get_context_data(**kwargs)
        fishs = Fish.objects.all()
        if not tmp['fl']:
            fishs = fishs.filter(start_fishing_date__lte=tmp['fishing_date'], finish_fishing_date__gte=tmp['fishing_date'])
        if not tmp['fl2']:
            fishs = fishs.filter(start_fishing_time__lte=tmp['fishing_time'], finish_fishing_time__gte=tmp['fishing_time'])
        if not tmp['fl3']:
            fishs = fishs.filter(start_spawning_date__lte=tmp['spawning_date'], finish_spawning_date__gte=tmp['spawning_date'])
        if not (tmp['lure'] == '0'):
            fsh_l = []
            for row in FishLure.objects.filter(lure=int(tmp['lure'])):
                fsh_l.append(row.fish_id)
            fishs = fishs.filter(fish_id__in=fsh_l)

        if not (tmp['bait'] == '0'):
            fsh_b = []
            for row in FishBait.objects.filter(bait=int(tmp['bait'])):
                fsh_b.append(row.fish_id)
            fishs = fishs.filter(fish_id__in=fsh_b)

        context['fish']=fishs
        return context

    def get_success_url(self):
        return reverse('main')

    def post(self, request, *args, **kwargs):
        self.post_context['fishing_date_month'] = self.request.POST['fishing_date_month']
        self.post_context['fishing_date_day'] = self.request.POST['fishing_date_day']
        self.post_context['spawning_date_month'] = self.request.POST['spawning_date_month']
        self.post_context['spawning_date_day'] = self.request.POST['spawning_date_day']
        self.post_context['fishing_time_hour'] = self.request.POST['fishing_time_hour']
        self.post_context['fishing_time_minute'] = self.request.POST['fishing_time_minute']
        self.post_context['fl'] = self.request.POST.get('fl','')
        self.post_context['fl2'] = self.request.POST.get('fl2','')
        self.post_context['fl3'] = self.request.POST.get('fl3','')
        self.post_context['bait'] = self.request.POST['bait']
        self.post_context['lure'] = self.request.POST['lure']
        return super(MainPage, self).post(request, *args, **kwargs)

    def get_initial(self):
        spawning_date_year = 2000
        spawning_date_month = self.post_context.get('spawning_date_month', datetime.today().month)
        spawning_date_day = self.post_context.get('spawning_date_day', datetime.today().day)
        fishing_date_year = 2000
        fishing_date_month = self.post_context.get('fishing_date_month', datetime.today().month)
        fishing_date_day = self.post_context.get('fishing_date_day', datetime.today().day)

        initial = super(MainPage, self).get_initial()
        initial['fishing_date'] = datetime(int(fishing_date_year), int(fishing_date_month), int(fishing_date_day), 0, 0, 0)
        initial['spawning_date'] = datetime(int(spawning_date_year), int(spawning_date_month), int(spawning_date_day), 0, 0, 0)
        initial['fishing_time'] = time(int(self.post_context.get('fishing_time_hour', 0)),int(self.post_context.get('fishing_time_minute', 0)))
        initial['fl'] = self.post_context.get('fl', 'true')
        initial['fl2'] = self.post_context.get('fl2', 'true')
        initial['fl3'] = self.post_context.get('fl3', 'true')
        initial['bait'] = self.post_context.get('bait', 0)
        initial['lure'] = self.post_context.get('lure', 0)
        return initial


class AddFish(CreateView):
    template_name = 'add_fish.html'
    form_class = FishForm
    model = Fish

    def get_success_url(self):
        return reverse('main')

class EditFish(UpdateView):
    template_name = 'edit_fish.html'
    form_class = FishForm
    model = Fish

    def get_success_url(self):
        return reverse('main')

class DeleteFish(View):
    def get(self, *args, **kwargs):
        fish = Fish.objects.get(pk=self.kwargs['pk'])
        if FishLure.objects.filter(fish=self.kwargs['pk']):
            for row in FishLure.objects.filter(fish=self.kwargs['pk']):
                row.delete()
            # raise PermissionDenied(u'Удалите все связи рыбы с прикормкой для удаления рыбы')
        if FishBait.objects.filter(fish=self.kwargs['pk']):
            for row in FishBait.objects.filter(fish=self.kwargs['pk']):
                row.delete()
            # raise PermissionDenied(u'Удалите все связи рыбы с приманкой для удаления рыбы')
        # else:
        fish.delete()
        return HttpResponseRedirect(reverse('main'))

class DetailFish(TemplateView):
    template_name = 'detail_fish.html'

    def get_context_data(self, **kwargs):
        context = super(DetailFish, self).get_context_data(**kwargs)
        context['bait'] = FishBait.objects.filter(fish=kwargs['pk'])
        context['lure'] = FishLure.objects.filter(fish=kwargs['pk'])
        context['fish'] = Fish.objects.get(pk=kwargs['pk'])
        context['start_fishing_date'] = calendar.month_name[context['fish'].start_fishing_date.month]
        context['finish_fishing_date'] = calendar.month_name[context['fish'].finish_fishing_date.month]
        context['start_spawning_date'] = calendar.month_name[context['fish'].start_spawning_date.month]
        context['finish_spawning_date'] = calendar.month_name[context['fish'].finish_spawning_date.month]
        return context



class BaitPage(TemplateView):
    template_name = 'bait.html'

    def get_context_data(self, **kwargs):
        context = super(BaitPage, self).get_context_data(**kwargs)
        context['bait'] = Bait.objects.all()
        return context

class AddBait(CreateView):
    template_name = 'add_bait.html'
    form_class = BaitForm
    model = Bait

    def get_success_url(self):
        return reverse('bait')

class EditBait(UpdateView):
    template_name = 'edit_bait.html'
    form_class = BaitForm
    model = Bait

    def get_success_url(self):
        return reverse('bait')

class DeleteBait(View):
    def get(self, *args, **kwargs):
        bait = Bait.objects.get(pk=self.kwargs['pk'])
        if FishBait.objects.filter(bait=self.kwargs['pk']):
            raise PermissionDenied(u'Удалите все связи рыбы с приманкой для удаления приманки')
        else:
            bait.delete()
            return HttpResponseRedirect(reverse('bait'))



class LurePade(TemplateView):
    template_name = 'lure.html'

    def get_context_data(self, **kwargs):
        context = super(LurePade, self).get_context_data(**kwargs)
        context['lure'] = Lure.objects.all()
        return context

class AddLure(CreateView):
    template_name = 'add_lure.html'
    form_class = LureForm
    model = Lure

    def get_success_url(self):
        return reverse('lure')

class EditLure(UpdateView):
    template_name = 'edit_lure.html'
    form_class = LureForm
    model = Lure

    def get_success_url(self):
        return reverse('lure')

class DeleteLure(View):
    def get(self, *args, **kwargs):
        lure = Lure.objects.get(pk=self.kwargs['pk'])
        if FishLure.objects.filter(lure=self.kwargs['pk']):
            raise PermissionDenied(u'Удалите все связи рыбы с прикормкой для удаления прикормки')
        else:
            lure.delete()
            return HttpResponseRedirect(reverse('lure'))



class FishLurePade(TemplateView):
    template_name = 'fishlure.html'

    def get_context_data(self, **kwargs):
        context = super(FishLurePade, self).get_context_data(**kwargs)
        context['fishlure'] = FishLure.objects.all()
        return context

class FishAddLure(CreateView):
    template_name = 'add_fishlure.html'
    form_class = FishLureForm
    model = FishLure

    def get_success_url(self):
        return reverse('fishlure')

class FishEditLure(UpdateView):
    template_name = 'edit_fishlure.html'
    form_class = FishLureForm
    model = FishLure

    def get_success_url(self):
        return reverse('fishlure')

class FishDeleteLure(View):
    def get(self, *args, **kwargs):
        fishlure = FishLure.objects.get(pk=self.kwargs['pk'])
        fishlure.delete()
        return HttpResponseRedirect(reverse('fishlure'))



class FishBaitPage(TemplateView):
    template_name = 'fishbait.html'

    def get_context_data(self, **kwargs):
        context = super(FishBaitPage, self).get_context_data(**kwargs)
        context['fishbait'] = FishBait.objects.all()
        return context

class FishAddBait(CreateView):
    template_name = 'add_fishbait.html'
    form_class = FishBaitForm
    model = FishBait

    def get_success_url(self):
        return reverse('fishbait')

class FishEditBait(UpdateView):
    template_name = 'edit_fishbait.html'
    form_class = FishBaitForm
    model = FishBait

    def get_success_url(self):
        return reverse('fishbait')

class FishDeleteBait(View):
    def get(self, *args, **kwargs):
        bait = FishBait.objects.get(pk=self.kwargs['pk'])
        bait.delete()
        return HttpResponseRedirect(reverse('fishbait'))